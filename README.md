Located in the Phoenix Flower District, the Arizona Flower Market is the premier place to buy bulk wholesale flowers for your wedding or special event. We provide customers with the opportunity to purchase fresh flowers, greens, and other floral supplies direct from the farms and manufacturers.

Address: 2050 South 16th Street, Suite 105, Phoenix, AZ 85034

Phone: 602-707-6294
